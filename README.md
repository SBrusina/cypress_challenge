1. Clone repository
2. In the project folder run -npm install
3. Install Cypress according to https://docs.cypress.io/guides/getting-started/installing-cypress
4. To run test execute -npx cypress open
5. Select "E2E Testing"
6. Click on "Start E2E Testing in Chrome"
7. Select file cypress\e2e\tests\playgroundTest.cy.js