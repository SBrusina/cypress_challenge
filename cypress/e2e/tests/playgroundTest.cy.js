import basePage from "../../pages/basePage"
import homePage from "../../pages/homePage"
import overlappedPage from "../../pages/overlappedPage"
import ajaxPage from "../../pages/ajaxPage"
import dynamicTablePage from "../../pages/dynamicTablePage"
import progressbarPage from "../../pages/progressbarPage"
import visibilityPage from "../../pages/visibilityPage"
import sampleappPage from "../../pages/sampleappPage"
import textInputPage from "../../pages/textInputPage"

const hostName = "http://localhost:3000/"

describe('UI Test Automation Playground', () => {

  beforeEach(() => {
    cy.visit(hostName)
  })

    it('Overlapped element', () => {
      homePage.getOverlappedLink().click();
      basePage.enterTextToField(overlappedPage.getIdField(), "Cypress test");
      basePage.enterTextToField(overlappedPage.getNameField(), "Cypress test");
      basePage.verifyFieldValue(overlappedPage.getIdField(), "Cypress test");
      basePage.verifyFieldValue(overlappedPage.getNameField(), "Cypress test");
    })

     it('AJAX Data', () => {
      homePage.getAjaxLink().click();
      ajaxPage.getAjaxButton().click();
      ajaxPage.waitForAjaxData();
      basePage.verifyElementText(ajaxPage.getLabel(), "Data loaded with AJAX get request.");
    })

    it('Visibility Visibility Hidden button', () => {
      homePage.getVisibilityLink().click();
      visibilityPage.getHideButton().click();
      basePage.verifyButtonIsHidden(visibilityPage.getInvisibleButton());
    })

    it('Visibility Display None button', () => {
      homePage.getVisibilityLink().click();
      visibilityPage.getHideButton().click();
      basePage.verifyButtonVisibility(visibilityPage.getNotDisplayedButton());
    })

    it('Visibility Offscreen button', () => {
      homePage.getVisibilityLink().click();
      visibilityPage.getHideButton().click();
      basePage.verifyButtonIsOffscreen(visibilityPage.getOffscreenButton());
    })

    it('Visibility Removed button', () => {
      homePage.getVisibilityLink().click();
      visibilityPage.getHideButton().click();
      basePage.verifyButtonExist(visibilityPage.getRemovedButton());
    })

    it('Visibility Zero Width button', () => {
      homePage.getVisibilityLink().click();
      visibilityPage.getHideButton().click();
      basePage.verifyButtonVisibility(visibilityPage.getZeroWidthButton());
    })

    it('Visibility Overlapped button', () => {
      homePage.getVisibilityLink().click();
      visibilityPage.getHideButton().click();
      basePage.verifyButtonIsOverlapped(visibilityPage.getOverlappedButton());
    })

    it('Visibility Opacity 0 button', () => {
      homePage.getVisibilityLink().click();
      visibilityPage.getHideButton().click();
      basePage.verifyButtonVisibility(visibilityPage.getInvisibleButton());
    })

    it('Dynamic Table', () => {
      homePage.getDynamicTableLink().click();
      dynamicTablePage.compareCPUForChrome();
    })

    it('Sample App positive test', () => {
      homePage.getSampleAppLink().click();
      basePage.enterTextToField(sampleappPage.getUserNameField(), "test");
      basePage.enterTextToField(sampleappPage.getPasswordField(), "pwd");
      sampleappPage.getLoginButton().click();
      basePage.verifyElementText(sampleappPage.getLoginStatus(), "Welcome, test!");
    })

    it('Sample App negative test', () => {
      homePage.getSampleAppLink().click();
      basePage.enterTextToField(sampleappPage.getUserNameField(), "test");
      basePage.enterTextToField(sampleappPage.getPasswordField(), "test_pwd");
      sampleappPage.getLoginButton().click();
      basePage.verifyElementText(sampleappPage.getLoginStatus(), "Invalid username/password");
    })

    it('Text Input', () => {
      homePage.getTextInputLink().click();
      basePage.enterTextToField(textInputPage.getMyButtonField(), "TestButton");
      textInputPage.getUpdatingButton().click();
      basePage.verifyElementText(textInputPage.getUpdatingButton(), "TestButton");
    })

    it('Progress Bar', () => {
      homePage.getProgressBarLink().click();
      progressbarPage.getStartButton().click();
      progressbarPage.waitForElement();
      progressbarPage.getStopButton().click();
      basePage.verifyElementText(progressbarPage.getResult(), "Result: 0");
    })
  })