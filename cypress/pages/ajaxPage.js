export default class ajaxPage {

  getAjaxButton() {
    return cy.get('#ajaxButton')
  }

  getLabel() {
    return cy.get('#content')
  }

  //wait more than 15 seconds(16) for element
  waitForAjaxData() {
    cy.get('#content', { timeout: 16000 }).should('be.visible')
}
}
module.exports = new ajaxPage();
require('cypress-xpath');