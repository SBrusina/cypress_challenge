export default class basePage {
//class conatains general operations to be used on any page

    enterTextToField(field, text) {
        field.scrollIntoView().type(text)
        }

    //extract value from filled field and compare it with expected
    verifyFieldValue(field, text) {
        field.scrollIntoView()
            .invoke('val')
            .should('contains', text)
        }

    verifyElementText(element, text) {
        element.invoke('text')
            .should('contains', text)
        }

    verifyButtonVisibility(buttonName){
        buttonName.should('not.visible')
        }

    verifyButtonExist(buttonName){
        buttonName.should('not.exist')
        }

    //verify if element style has visibility: hidden
    verifyButtonIsHidden(buttonName){
        buttonName.invoke('attr', 'style').should('equal', 'visibility: hidden;');
        }

    //verify if element class has offscreen value
    verifyButtonIsOffscreen(buttonName){
        buttonName.invoke('attr', 'class').should('contain', 'offscreen');
        }

   verifyButtonIsOverlapped(elementName){
        cy.once('fail', (e) => {
            // we expect the click to fail with this message
            expect(e.message).to.include('is being covered by another element:');
        })
        elementName.click();
    }
  }
  module.exports = new basePage();
  require('cypress-xpath');