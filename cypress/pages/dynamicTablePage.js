export default class dynamicTablePage{

    elements = {
      columnsTitles : () => cy.xpath('//div[@role="rowgroup"]//span[@role="columnheader"]'),
      chromeCells : () => cy.xpath('//span[contains(text(), "Chrome")]/following-sibling::span'),
      chromeCpu : () => cy.get('//*[contains(text(), "Chrome CPU")]')
    }

    compareCPUForChrome() {
      //get array of table titles
      cy.xpath('//div[@role="rowgroup"]//span[@role="columnheader"]').each(($ele, index) => {
        //find title with CPU name
        if ($ele.text().includes('CPU')) {
          //extract Chrome CPU value from yellow label
          cy.xpath('//*[contains(text(), "Chrome CPU")]').invoke('text').then(text => {
            //remenber value from yellow label to let
            let cpu = text.split(': ')[1];
            //get value from table for cell which is corespond to CPU column
            cy.xpath('//span[contains(text(), "Chrome")]/following-sibling::span').eq(index-1).invoke('text').should('contains', cpu);
          });
        }
      })
    }
  }
  module.exports = new dynamicTablePage();
  require('cypress-xpath');
