export default class homePage {

    getOverlappedLink() {
            return cy.xpath(`//a[contains(text(), 'Overlapped Element')]`)
    }

    getAjaxLink() {
        return cy.xpath(`//a[contains(text(), 'AJAX Data')]`)
    }

    getVisibilityLink() {
        return cy.xpath(`//a[contains(text(), 'Visibility')]`)
    }

    getDynamicTableLink() {
        return cy.xpath(`//a[contains(text(), 'Dynamic Table')]`)
    }

    getSampleAppLink() {
        return cy.xpath(`//a[contains(text(), 'Sample App')]`)
    }

    getTextInputLink() {
        return cy.xpath(`//a[contains(text(), 'Text Input')]`)
    }

    getProgressBarLink() {
        return cy.xpath(`//a[contains(text(), 'Progress Bar')]`)
    }
}
module.exports = new homePage();
require('cypress-xpath');