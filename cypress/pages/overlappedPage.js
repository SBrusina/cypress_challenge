export default class overlappedPage {

    getIdField() {
            return cy.get("#id")
    }

    getNameField() {
        return cy.get("#name")
    }
}
module.exports = new overlappedPage();