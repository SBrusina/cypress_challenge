export default class progressbarPage{

  getStartButton() {
    return cy.get('#startButton')
  }

  getStopButton() {
    return cy.get('#stopButton')
  }

  getResult() {
    return cy.get('#result')
  }

  //wait for element with value 75
  waitForElement() {
      cy.xpath(`//div[@aria-valuenow="75"]`, { timeout: 25000 }).should('be.visible')
  }
}
module.exports = new progressbarPage();
require('cypress-xpath');