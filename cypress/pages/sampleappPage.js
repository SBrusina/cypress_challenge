export default class sampleappPage {

    getUserNameField() {
         return cy.xpath('//input[@name="UserName"]')
    }

    getPasswordField() {
        return cy.xpath('//input[@name="Password"]')
   }

    getLoginButton() {
        return cy.get('#login')
    }

    getLoginStatus() {
        return cy.get('#loginstatus')
    }
}
module.exports = new sampleappPage();
require('cypress-xpath');