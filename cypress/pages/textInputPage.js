export default class textInputPage{

    getMyButtonField() {
      return cy.get('#newButtonName')
    }

    getUpdatingButton() {
      return cy.get('#updatingButton')
    }
}
module.exports = new textInputPage();