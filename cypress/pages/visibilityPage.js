export default class visibilityPage {

    getHideButton() {
            return cy.get('#hideButton')
    }

    getRemovedButton() {
        return cy.get('#removedButton')
    }

    getZeroWidthButton() {
        return cy.get('#zeroWidthButton')
    }

    getOverlappedButton() {
        return cy.get('#overlappedButton')
    }

    getTransparentButton() {
        return cy.get('#transparentButton')
    }

    getInvisibleButton() {
        return cy.get('#invisibleButton')
    }

    getNotDisplayedButton() {
        return cy.get('#notdisplayedButton')
    }

    getOffscreenButton() {
        return cy.get('#offscreenButton')
    }
}
module.exports = new visibilityPage();
require('cypress-xpath');